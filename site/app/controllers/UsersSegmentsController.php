<?php

namespace App\Controllers;

use  App\Controllers\ControllerBase;
use Innovationdothome\UsersSegments\UsersSegments;

class UsersSegmentsController extends UsersSegments
{

    public function initialize()
    {
      # TO DO - check access to this superadmin module
        
      parent::initialize();
      
      $this->view->setViewsDir($this->config->modules->users_segments->viewsDir);
    }
    
    function onConstruct()
    {
        $base = new ControllerBase();
        
        $base->onConstruct();
        
    }    
    
    public function indexAction()
    {

    }
    
    
}