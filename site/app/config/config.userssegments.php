<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') || define('APP_PATH', BASE_PATH . ''.DS.'app');

return new \Phalcon\Config([
    
    'modules' => [
        
      # ADD THIS PART IN YOUR MAIN CONFIG and remove this file from you root config dir NOT FROM VENDOR DIR (!)  
      'users_segments' => [
          'viewsDir'       => APP_PATH .DS.'views'.DS.'usersSegments',
        ]   

    ],
    
]);

