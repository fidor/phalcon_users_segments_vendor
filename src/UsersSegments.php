<?php

namespace Innovationdothome\UsersSegments;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use App\Models\Users;

abstract class UsersSegments extends Controller
{

    protected $dataReturn = true;

    public function initialize()
    {
        if (!SupermoduleBase::checkAndConnectModule('users_segments')) {
            $this->dataReturn = false;
        }
    }
    
    public function onConstruct()
    {
       
    }

    public function getUsersByParams($params = null)
    {
        return Users::find($params);
    }
}

